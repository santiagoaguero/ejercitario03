<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 1</title>
</head>
<body>
  <?php
    $var_dos = 2; //variable para el numero 2
    $var_tres = 3; //variable para le número 3
    $raiz_cubica = 1/3; //raiz cubica

    $x = ((sqrt($var_dos) * pi()) + pow($var_tres, $raiz_cubica)) / (M_EULER * M_E);

    echo '<b>El resultado de la fórmula x = ((A * ¶) + B) / (C*D) es: </b>' . $x . '</br></br>';
    echo '<b>Donde:</b>';
    echo '<ul>
            <li>A es la raiz cuadrada de 2.</li>
            <li>¶ es el número PI.</li>
            <li>B es es la raíz cúbica de 3.</li>
            <li>C es la constante de Euler.</li>
            <li>D es la constante e.</li>
          </ul>'
  ?>
</body>
</html>
