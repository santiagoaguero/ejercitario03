<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ejercicio 2</title>
</head>
<body>
  <?php
    $version = phpversion();
    $id_version = PHP_VERSION_ID;
    $max_enteros = PHP_INT_MAX;
    $max_nombre = PHP_MAXPATHLEN;
    $so = PHP_OS;
    $simbolo_fin = PHP_EOL;
    $include_path = DEFAULT_INCLUDE_PATH;

    echo '<ul>
            <li><b>Versión de PHP utilizada:</b> ' . $version . '</li>
            <li><b>El ID de la versión de PHP:</b> ' . $id_version . '</li>
            <li><b>El valor máximo soportado para enteros para esa versión:</b> ' . $max_enteros . '</li>
            <li><b>Tamaño máximo del nombre de un archivo:</b> ' . $max_enteros . '</li>
            <li><b>Versión del Sistema Operativo:</b> ' . $so . '</li>
            <li><b>Símbolo correcto de "Fin De Línea" para la plataforma en uso:</b> ' . $simbolo_fin . '</li>
            <li><b>El include path por defecto:</b> ' . $include_path . '</li>
          </ul>';
  ?>
</body>
</html>