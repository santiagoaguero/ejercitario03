<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>
<body>
    <?php
        function crear_semilla()
        {
          list($usec, $sec) = explode(' ', microtime());
          return (float) $sec + ((float) $usec * 100000);
        }
        mt_srand(crear_semilla());
        
        $v1 = mt_rand(50, 900);
        $v2 = mt_rand(50, 900);
        $v3 = mt_rand(50, 900);
        $aleatorios = array($v1, $v2, $v3); //llenamos el array con los numeros generados aleatoriamente

        rsort($aleatorios); //ordenamos el array descendentemente

        $v3 = array_pop($aleatorios);
        $v2 = array_pop($aleatorios);
        $v1 = array_pop($aleatorios);

        echo '<span style="background:green; color:white; font-size:30px">' . $v1 . '</span> ';
        echo '<span style="font-size:30px">' . $v2 . '</span> ';
        echo '<span style="background:red; color:white; font-size:30px">' . $v3 . '</span>';
    ?>
</body>
</html>