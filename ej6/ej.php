<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 6</title>
    <script>
        <?php
            function calcular() {
                error_reporting(E_ALL ^ E_NOTICE);
                        $operando1 = $_POST['operando1'];
                $operando2 = $_POST['operando2'];
                $operacion = $_POST['operacion'];
                switch ($operacion) {
                    case 'sumar':
                        $resultado = $operando1 + $operando2;
                        break;
                    case 'restar':
                        $resultado = $operando1 - $operando2;
                        break;
                    case 'multiplicar':
                        $resultado = $operando1 * $operando2;
                        break;
                    case 'dividir':
                        if ($operando2 == 0) {
                            $resultado = 'No se puede dividir un número entre 0!!';
                        }
                        else {
                            $resultado = $operando1 / $operando2;
                        }
                        break;
                    default:
                        break;
                }
                $tipo_resultado = gettype($resultado);
                if ($tipo_resultado == 'string') {
                    echo '<div style="margin-top:10px"><b>' . $resultado . '</b></div>';
                }
                else {
                    echo '<div style="margin-top:10px; text-decoration:underline">El resultado es: <b>' . $resultado . '</b></div>';
                }
            }
        ?>
    </script>
</head>
<body>
    <form action="ej.php" method="post">
        <p>
            <label for="operando1">Inserte el Operando 1:</label>     
            <input type="number" name="operando1" id="operando1">
        </p>
        <p>
            <label for="operando2">Inserte el Operando 2:</label>
            <input type="number" name="operando2" id="operando2">
        </p>

        <select name="operacion" id="operacion">
            <option value="sumar">Sumar</option>
            <option value="restar">Restar</option>
            <option value="multiplicar">Multiplicar</option>
            <option value="dividir">Dividir</option>
        </select>
        <input type="submit" value="Calcular" onclick="calcular()">
    </form>


    <?php
        echo calcular();
    ?>


</body>
</html>
