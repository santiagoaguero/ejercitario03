<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7</title>
</head>
<body>


    <?php
        $str=<<<HTML
            <form action="#" method="post">
                <div>
                    <label for="palabra">Verificar si es Palindromo:</label>
                        <input type="text" name="palabra" placeholder="Escriba una palabra" />
                </div>
                <br/>
                <div class="button">
                    <button type="submit">Verificar</button>
                </div>
            </form>
        HTML;

        if( !isset($_POST['palabra']))
        {
            echo $str;
        } else {
            echo $largo = "";
            $palabra = $_POST['palabra'];        
            $resultado = verificarPalindromo($palabra);
            echo "Es Palindromo?: ";
            echo "<b>$resultado</b>";
            echo "<br/><br/><a href='ej.php'>Volver al Verificador</a><br/>";
        }


//



        function verificarPalindromo($palabra)
        {
            $largo = strlen($palabra) - 1;
            if ($largo >= 2) 
            {
                for ($i = 0; $i <= $largo; $i++)
                {
                    if (!(substr($palabra, $i, 1) == substr($palabra, ($largo - $i), 1)))
                    {
                        return "NO";
                        break;
                    }
                }
                return "SI";
            } else {
                return "Debe escribir una palabra de por lo menos 2 letras para verificar.";
            }
        }
    ?>
</body>
</html>